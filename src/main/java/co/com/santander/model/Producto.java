package co.com.santander.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Producto 
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idProducto;
    private String nombre;
    private Double precio;
    private Long cantidadStock;
    private String estado;
    private Date fechaCreacion;
      
	public Producto(Long idProducto, String nombre, Double precio, Long cantidadStock, String estado,Date fechaCreacion)
	{
		this.idProducto = idProducto;
		this.nombre = nombre;
		this.precio = precio;
		this.cantidadStock = cantidadStock;
		this.estado = estado;
		this.fechaCreacion = fechaCreacion;
	}
	
	public Producto(String nombre, Double precio) {
		super();
		this.nombre = nombre;
		this.precio = precio;
	}



	public Producto() {}
	
	
	public Long getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(Long idProducto) {
		this.idProducto = idProducto;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Double getPrecio() {
		return precio;
	}
	public void setPrecio(Double precio) {
		this.precio = precio;
	}
	public Long getCantidadStock() {
		return cantidadStock;
	}
	public void setCantidadStock(Long cantidadStock) {
		this.cantidadStock = cantidadStock;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
}
