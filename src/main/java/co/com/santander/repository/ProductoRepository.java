package co.com.santander.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.santander.model.Producto;

public interface ProductoRepository extends JpaRepository<Producto, Integer> {
}
