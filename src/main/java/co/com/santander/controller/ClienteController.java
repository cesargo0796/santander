package co.com.santander.controller;

import java.time.LocalDateTime;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.com.santander.model.Cliente;
import co.com.santander.repository.ClienteRepository;
import co.com.santander.util.Util;

@RestController
@RequestMapping("/Santander/Clientes/V1.0")
public class ClienteController {
    
    @Autowired
    private ClienteRepository clienteRepository;
    
    private static final Logger LOGGER = LogManager.getRootLogger();

	@GetMapping(path = "/ConsultarCliente", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<?> getClients(@RequestParam (value = "nombre", required = false) String nombre) 
    		throws Exception 
	{
		LOGGER.info("Inicio de consulta de clientes");
		if(Util.validarCampoNulo(nombre))
		{
			return clienteRepository.findAll();			
		}
		else
		{
			return clienteRepository.findByNombre(nombre);
			
		}	
    }
	
	@PostMapping(path = "/CrearCliente", produces = MediaType.APPLICATION_JSON_VALUE)
    public String createClient(
    		@RequestParam (value = "nombre", required = false) String nombre,
    		@RequestParam (value = "cedula", required = false) Long cedula,
    		@RequestParam (value = "producto", required = false) String producto) 
    		throws Exception 
	{
		LOGGER.info("Inicio de consulta de clientes");
		if(Util.validarCampoNulo(nombre))		
			return "Por favor ingresar nombre";			
		
		if(cedula == null)		
			return "Por favor ingresar cedula";			
		
		if(Util.validarCampoNulo(producto))		
			return "Por favor ingresar producto";			
		
		Cliente client=new Cliente();
		client.setCedula(cedula);
		client.setNombre(nombre);
		client.setProducto(producto);
		client.setFechaRegistro(LocalDateTime.now());
		clienteRepository.save(client);
		return "Cliente creado exitosamente";
				
		
    }
	
	@PutMapping(path = "/ActualizarCliente", produces = MediaType.APPLICATION_JSON_VALUE)
    public String updateClient(
    		@RequestParam (value = "cedula", required = false) Long cedula,
    		@RequestParam (value = "nombre", required = false) String nombre)
    		throws Exception
	{
		LOGGER.info("Inicio de consulta de productos en carrito");
		if(cedula == null || Util.validarCampoNulo(nombre))
		{
			return "Por favor ingresar cedula y nombre de cliente";
		}

        int updateCode=clienteRepository.updateCliente(cedula, nombre);
        if(updateCode != 0)        
        	return "Cliente actualizado exitosamente";
        else
        	return "Cliente no existe";        	
    }
	
	@DeleteMapping(path = "/EliminarCliente", produces = MediaType.APPLICATION_JSON_VALUE)
    public String deleteClient(@RequestParam (value = "cedula", required = false) Long cedula) 
    		throws Exception
	{
		LOGGER.info("Inicio de Eliminación de cliente {}",cedula);
		
		if(cedula == null)
		{
			return "Por favor ingresar cedula de cliente";
		}

		int deleteCode=clienteRepository.deleteByCedulaCliente(cedula);
        if(deleteCode != 0)        
        	return "Cliente eliminado exitosamente";
        else
        	return "Cliente no existe";       
    }

	
	@DeleteMapping(path = "/LimpiarClientes", produces = MediaType.APPLICATION_JSON_VALUE)
    public String cleanClients() 
    		throws Exception
	{
		LOGGER.info("Inidio de Depuración de clientes");

		clienteRepository.deleteAll();

        return "Depuracion de clientes terminada";
    }
}
