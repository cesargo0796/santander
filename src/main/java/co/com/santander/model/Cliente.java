package co.com.santander.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Cliente 
{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long idCliente;
    private String nombre;
    private Long cedula;
    private String producto;
    private LocalDateTime fechaRegistro;
      
    
	
	public Cliente(Long idCliente, String nombre, Long cedula, String producto, LocalDateTime fechaRegistro) {
		this.idCliente = idCliente;
		this.nombre = nombre;
		this.cedula = cedula;
		this.producto = producto;
		this.fechaRegistro = fechaRegistro;
	}



	public Cliente() {}



	public Long getIdCliente() {
		return idCliente;
	}



	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}



	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public Long getCedula() {
		return cedula;
	}



	public void setCedula(Long cedula) {
		this.cedula = cedula;
	}



	public String getProducto() {
		return producto;
	}



	public void setProducto(String producto) {
		this.producto = producto;
	}



	public LocalDateTime getFechaRegistro() {
		return fechaRegistro;
	}



	public void setFechaRegistro(LocalDateTime fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
}
