package co.com.santander.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import co.com.santander.model.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Long> 
{
	public List<Cliente> findByNombre(String nombre);
	@Transactional
    @Modifying
	@Query("delete from Cliente c where c.cedula = :cedula")
	public int deleteByCedulaCliente(@Param(value = "cedula") long cedula);	
	@Transactional
	@Modifying
	@Query("update Cliente c set c.nombre = :nombre where c.cedula = :cedula")
	public int updateCliente(@Param(value = "cedula") long cedula, @Param(value = "nombre") String nombre);
}
